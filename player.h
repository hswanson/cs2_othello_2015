#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <cstddef>

#include "common.h"
#include "board.h"

class Player
{
  public:
    /** Constructs a player with an empty internal board. */
    Player(const Side& side);

    /** Standard destructor. */
    ~Player();

    /** Sets the player's internal board. */
    void setBoard(const Board& b);

    /**
     * Given an opponent's move, return a replying move, and applies both moves
     * to the internal board. The caller is responsible for deleting the
     * returned move.
     *
     * msLeft represents the time the player has left for the total game, in
     * milliseconds. doMove() must take no longer than msLeft, or this player
     * will be disqualified! An msLeft value of -1 indicates no time limit.
     */
    Move* doMove(Move *opponentsMove, int msLeft);

    /** True if this player is being tested by testMinimax */
    bool testingMinimax;

  private:
    /** TODO comment well */
    Move* root_minimax(int depth);

    /** TODO comment well */
    int minimax(const Board& board, int depth, int alpha, int beta, const Side& side);

    /** The side the player plays for. */
    const Side side_;

    /** The internal board. */
    Board board_;

    /** The depth to search to. */
    int depth_;
};

#endif
