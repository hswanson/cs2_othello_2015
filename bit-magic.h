#ifndef __BIT_MAGIC_H__
#define __BIT_MAGIC_H__

#include <cstdint>

static const uint64_t CORNERS = 0x8100000000000081L;
static const uint64_t DANGER_SQUARES = 0x42C300000000C342L;

// This needs some explanation. There are 8 directions we can 'capture' in.
// The DIRECTIONS array encodes the direction as an integer (+x = 1, +y = 8).
// The BORDERS array contains 64-bit integers, where certain bits are set:
//   if moving along DIRECTIONS[i] from the jth square would take you off the
//   the board, the ith bit of BORDERS[j] is set. E.g., BORDERS[0] has the
//   right edge of the board set, because DIRECTION[0] is 'move right'.
static const int DIRECTIONS [8] = {1, -1, 8, -8, 7, -7, 9, -9};
static const uint64_t BORDERS [8] = {0x8080808080808080L,
                                     0x0101010101010101L,
                                     0xFF00000000000000L,
                                     0x00000000000000FFL,
                                     0xFF01010101010101L,
                                     0x80808080808080FFL,
                                     0xFF80808080808080L,
                                     0x01010101010101FFL};

static const uint64_t ONE_L = 1L;

static inline uint64_t singleBit(int i)
{
    return (ONE_L << i);
}

static inline bool checkBit(uint64_t val, int i)
{
    return val & singleBit(i); // long -> bool conversion works correctly
}

static inline int bitCount(uint64_t val)
{
    int total = 0;
    while (val != 0)
    {
        val &= (val - 1); // clear the LS1B
        total++;
    }

    return total;
}

static inline uint64_t shiftDir(uint64_t val, int dir_index)
{
    int dir = DIRECTIONS[dir_index];
    if(dir > 0)
        return (val & ~BORDERS[dir_index]) << dir;
    return (val & ~BORDERS[dir_index]) >> -dir;
}

#endif
