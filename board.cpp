#include "board.h"

// TODO comment the fuck out of this
static inline const uint64_t getMovesInDir(uint64_t gen, uint64_t prop, uint64_t term, int dir_index)
{
    // A WEST capture requires shifting bits EAST
    int opposite_dir = dir_index ^ 1;
    uint64_t temp = shiftDir(gen, opposite_dir) & prop;
    uint64_t streaks = temp;
    for(int i = 0; i < 7; i++)
    {
        temp = shiftDir(temp, opposite_dir) & prop;
        streaks |= temp;
    }
    return shiftDir(streaks, opposite_dir) & term;
}

Board::Board()
{
    blackBit_ = singleBit(28) | singleBit(35);
    whiteBit_ = singleBit(27) | singleBit(36);
}

Board::Board(const Board& copy)
{
    blackBit_ = copy.blackBit_;
    whiteBit_ = copy.whiteBit_;
}

Board& Board::operator=(const Board& other)
{
    blackBit_ = other.blackBit_;
    whiteBit_ = other.whiteBit_;
    return *this;
}

Board::~Board()
{
}

SquareType Board::get(int x, int y) const
{
    return get(x + 8 * y);
}

SquareType Board::get(int index) const
{
    if(checkBit(blackBit_, index))
        return BLACK_SQ;
    else if(checkBit(whiteBit_, index))
        return WHITE_SQ;
    else
        return EMPTY_SQ;
}

int Board::numFilled() const
{
    return bitCount(blackBit_ | whiteBit_);
}

GameState Board::getGameState(const Side& side) const
{
    uint64_t moves = 0;
    uint64_t unoccupied = ~(blackBit_ | whiteBit_);
    for(int i = 0; i < 8; i++)
    {
        moves |= getMovesInDir(blackBit_, whiteBit_, unoccupied, i);
        moves |= getMovesInDir(whiteBit_, blackBit_, unoccupied, i);
    }
    if(moves != 0) // if some side can move
        return IN_PROGRESS;

    int blackMinusWhite = bitCount(blackBit_) - bitCount(whiteBit_);
    if(blackMinusWhite > 0)
        return (side == BLACK) ? WIN : LOSS;
    else if(blackMinusWhite < 0)
        return (side == BLACK) ? LOSS : WIN;
    else
        return TIE;
}

void Board::set(SquareType sq, int x, int y)
{
    set(sq, x + 8 * y);
}

void Board::set(SquareType sq, int index)
{
    uint64_t mask = singleBit(index);
    switch(sq)
    {
      case WHITE_SQ:
        blackBit_ &= ~mask;
        whiteBit_ |=  mask;
        break;
      case BLACK_SQ:
        blackBit_ |=  mask;
        whiteBit_ &= ~mask;
        break;
      case EMPTY_SQ:
        blackBit_ &= ~mask;
        whiteBit_ &= ~mask;
        break;
    }
}

const std::list<Move> Board::getMoves(const Side& side) const
{
    uint64_t selfBits = (side == BLACK ? blackBit_ : whiteBit_);
    uint64_t otherBits = (side == BLACK ? whiteBit_ : blackBit_);

    std::list<Move> moves;
    uint64_t unoccupied = ~(blackBit_ | whiteBit_);

    uint64_t capture_points [8];
    uint64_t all_captures = 0;
    for(int i = 0; i < 8; i++)
    {
        capture_points[i] = getMovesInDir(selfBits, otherBits, unoccupied, i);
        all_captures |= capture_points[i];
    }

    for(int i = 0; i < 64; i++)
    {
        if(checkBit(all_captures, i))
        {
            Move m;
            m.position = i;
            m.dirs = 0;
            for(int j = 0; j < 8; j++)
                if(checkBit(capture_points[j], i))
                    m.dirs |= (1 << j);
            moves.push_back(m);
        }
    }

    return moves;
}

SquareType Board::toSquareType(const Side& side)
{
    return (side == WHITE) ? WHITE_SQ : BLACK_SQ;
}

SquareType Board::opposite(const SquareType& sq)
{
    return (sq == WHITE_SQ) ? BLACK_SQ : WHITE_SQ;
}

void Board::doMove(const Move& m, const Side& side)
{
    uint64_t selfBits = (side == BLACK ? blackBit_ : whiteBit_);
    uint64_t otherBits = (side == BLACK ? whiteBit_ : blackBit_);

    uint64_t toToggle = 0;

    unsigned char dirs = m.dirs;

    // If we get this from the wrapper, we won't have precomputed directions
    if(dirs == 0)
    {
        uint64_t singlePiece = singleBit(m.position);
        for(int i = 0; i < 8; i++)
        {
            // Now we're "moving" the other way (from new to old, so we flip i again)
            uint64_t move = getMovesInDir(singlePiece, otherBits, selfBits, i ^ 1);
            if(move != 0)
                dirs |= (1 << i);
        }
    }

    for(int i = 0; i < 8; i++)
    {
        if(dirs & (1 << i))
        {
            uint64_t seed = singleBit(m.position);
            uint64_t temp = seed;
            for(int j = 0; j < 7; j++)
            {
                temp = shiftDir(temp, i) & otherBits;
                toToggle |= temp;
            }
        }
    }

    // Regardless of which side we're on, we just need to flip the piece over,
    // so to speak.
    blackBit_ ^= toToggle;
    whiteBit_ ^= toToggle;

    // Lastly, put this piece on the board
    if(side == WHITE)
        whiteBit_ |= singleBit(m.position);
    else
        blackBit_ |= singleBit(m.position);
}

int Board::heuristic(const Side& side) const
{
    uint64_t unoccupied = ~(blackBit_ | whiteBit_);

    uint64_t blackMoves = 0;
    uint64_t whiteMoves = 0;

    for(int i = 0; i < 8; i++)
    {
        blackMoves |= getMovesInDir(blackBit_, whiteBit_, unoccupied, i);
        whiteMoves |= getMovesInDir(whiteBit_, blackBit_, unoccupied, i);
    }

    uint64_t blackFrontier = 0;
    uint64_t whiteFrontier = 0;
    for(int i = 0; i < 8; i++)
    {
        blackFrontier |= shiftDir(blackBit_, i) & unoccupied;
        whiteFrontier |= shiftDir(whiteBit_, i) & unoccupied;
    }

    // All computed from black's perspective
    int delta_move = bitCount(blackMoves) - bitCount(whiteMoves);
    int delta_frontier = bitCount(blackFrontier) - bitCount(whiteFrontier);
    int delta_corner = bitCount(blackBit_ & CORNERS) - bitCount(whiteBit_ & CORNERS);
    int delta_danger = bitCount(blackBit_ & DANGER_SQUARES) - bitCount(whiteBit_ & DANGER_SQUARES);

    int blackScore = delta_move - (delta_frontier << 3) + (delta_corner << 7) - (delta_danger << 3);
    return (side == BLACK ? blackScore : -blackScore);
}
