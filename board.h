#ifndef __BOARD_H__
#define __BOARD_H__

#include <list>

#include "bit-magic.h"
#include "common.h"

enum SquareType
{
    WHITE_SQ, BLACK_SQ, EMPTY_SQ
};

class Board
{
  public:
    /** Constructs an empty board. */
    Board();

    /** Copy constructor. */
    Board(const Board& copy);

    /** Assignment operator. */
    Board& operator=(const Board& other);

    /** Standard destructor. */
    ~Board();


    /** Returns the thing at the given location. */
    SquareType get(int x, int y) const;

    /** Returns the thing at the given index. */
    SquareType get(int index) const;

    /** Returns the number of non-empty squares */
    int numFilled() const;

    /** Returns whether the given side has won, lost, tied, or if the game is
        still in progress. */
    GameState getGameState(const Side& side) const;


    /** Sets the given thing at the given location. */
    void set(SquareType sq, int x, int y);

    /** Sets the given thing at the given index. */
    void set(SquareType sq, int index);


    /** Returns a list of all legal moves. Will be empty if the only legal
        move is to pass. */
    const std::list<Move> getMoves(const Side& side) const;

    /** Applies the given move, as played by the given side. */
    void doMove(const Move& m, const Side& side);

    /** Returns an approximate value for the given player. */
    int heuristic(const Side& side) const;

  private:
    static SquareType toSquareType(const Side& side);
    static SquareType opposite(const SquareType& sq);

    // We use a bitboard representation, where (i, j) is at bit (i + 8 * j)
    uint64_t blackBit_;
    uint64_t whiteBit_;
};

#endif
