#ifndef __COMMON_H__
#define __COMMON_H__

#include <cstdint>

enum GameState
{
    WIN, LOSS, TIE, IN_PROGRESS
};

enum Side
{
    WHITE, BLACK
};

inline Side opposite(const Side& side)
{
    return (side == BLACK) ? WHITE : BLACK;
}

struct Move
{
    int position;
    unsigned char dirs;
};

#endif
