#include <cstdio>
#include "common.h"
#include "player.h"
#include "board.h"

// Use this file to test your minimax implementation (2-ply depth, with a
// heuristic of the difference in number of pieces).
int main(int argc, char *argv[])
{
    /** Desired board state:
     *  - - - - - - - -
     *  - - - - - - - -
     *  - b - - - - - -
     *  b w b b b b - -
     *  - - - - - - - -
     *  - - - - - - - -
     *  - - - - - - - -
     *  - - - - - - - -
     */
    Board *board = new Board();
    board->set(EMPTY_SQ, 3, 3);
    board->set(EMPTY_SQ, 3, 4);
    board->set(EMPTY_SQ, 4, 3);
    board->set(EMPTY_SQ, 4, 4);

    board->set(BLACK_SQ, 0, 3);
    board->set(BLACK_SQ, 1, 2);
    board->set(WHITE_SQ, 1, 3);
    board->set(BLACK_SQ, 2, 3);
    board->set(BLACK_SQ, 3, 3);
    board->set(BLACK_SQ, 4, 3);
    board->set(BLACK_SQ, 5, 3);

    // Initialize player as the white player
    Player *player = new Player(WHITE);
    player->testingMinimax = true;
    player->setBoard(*board);

    // Get player's move and check if it's right.
    Move *move = player->doMove(NULL, 0);

    if (move != NULL && move->position == 1 + 8 * 1) {
        printf("Correct move: (1, 1)\n");
    } else {
        printf("Wrong move: got ");
        if (move == NULL) {
            printf("PASS");
        } else {
            int x = move->position % 8;
            int y = move->position / 8;
            printf("(%d, %d)", x, y);
        }
        printf(", expected (1, 1)\n");
    }

    return 0;
}
