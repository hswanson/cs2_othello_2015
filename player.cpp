#include "player.h"

#include <climits>
#include <iostream>

#define INFTY (INT_MAX) // note! INT_MIN != -INFTY. Analogy: -128 and 127

//#define PRINT

static void printBoard(const Board& board)
{
#ifdef PRINT
    for(int i = 0; i < 8; i++)
    {
        for(int j = 0; j < 8; j++)
        {
            SquareType sq = board.get(j, i);
            char c;
            switch(sq)
            {
              case EMPTY_SQ: c = '-'; break;
              case BLACK_SQ: c = '@'; break;
              case WHITE_SQ: c = 'O'; break;
            }
            std::cerr << c;
        }
        std::cerr << std::endl;
    }
    std::cerr << std::endl;
#endif
}

static void printMove(const Move* m)
{
#ifdef PRINT
    if(m != NULL)
        std::cerr << "pos: " << m->position << " dirs: " << (int) m->dirs << std::endl;
    else
        std::cerr << "NULL" << std::endl;
#endif
}

Player::Player(const Side& side) : testingMinimax(false), side_(side), board_(), depth_(8)
{
}

Player::~Player()
{
}

void Player::setBoard(const Board& b)
{
    board_ = b;
}

Move *Player::doMove(Move *opponentsMove, int msLeft)
{
    static int prevMs = 0;
    // if we are timed and prevMs is valid
    if(msLeft > 0 && prevMs != 0)
    {
        int turns_left = (64 - board_.numFilled()) / 2;
        int deltaMs = prevMs - msLeft;
        if(deltaMs * turns_left > msLeft) // if we're running out of time
            depth_--;
        if(2 * deltaMs * turns_left < msLeft) // if we have time to spare
            depth_++;
    }

    prevMs = msLeft; // set the previous time

    std::cerr << "Time left: " << msLeft << std::endl;
    std::cerr << "Depth: " << depth_ << std::endl;

    if(opponentsMove != NULL) // they didn't pass
        board_.doMove(*opponentsMove, opposite(side_));

    printBoard(board_);
    printMove(opponentsMove);

    Move* m;
    if(testingMinimax)
        m = root_minimax(2);
    else
        m = root_minimax(depth_);

    if(m != NULL)
        board_.doMove(*m, side_);

    printBoard(board_);
    printMove(m);

    return m;
}

Move* Player::root_minimax(int depth)
{
    int alpha = -INFTY;
    int beta = INFTY;
    Side other = opposite(side_);

    std::list<Move> moves = board_.getMoves(side_);
    if(moves.empty())
        return NULL;

    int best_val = -INFTY;
    Move best_move;
    best_move.position = 1337;
    for(auto it = moves.begin(); it != moves.end(); it++)
    {
        Board copy (board_);
        copy.doMove(*it, side_);
        int val = -minimax(copy, depth - 1, -beta, -alpha, other);
        if(val > best_val)
        {
            best_val = val;
            best_move = *it;
        }
        alpha = std::max(alpha, val);
    }

    // Allocates a move on the heap to avoid dangling pointers
    Move* m = new Move;
    *m = best_move;
    return m;
}

// alpha is the best score acheived by your older siblings, and also your children
// beta is the worst score acheived by the older siblings of your parent
int Player::minimax(const Board& board, int depth, int alpha, int beta, const Side& side)
{
    GameState gs = board.getGameState(side);
    switch(gs)
    {
      // not actually infinite, we need to be larger than best_val
      case WIN: return INFTY - 1;
      case LOSS: return -INFTY + 1;
      case TIE: return 0;
      case IN_PROGRESS:
        ; // empty
    } 

    if(depth == 0)
        return board.heuristic(side);

    Side other = opposite(side);

    std::list<Move> moves = board.getMoves(side);
    if(moves.empty()) // player must pass
    {
        return -minimax(board, depth, -beta, -alpha, other);
    }

    int best_val = -INFTY;
    for(auto it = moves.begin(); it != moves.end(); it++)
    {
        Board copy (board);
        copy.doMove(*it, side);
        int val = -minimax(copy, depth - 1, -beta, -alpha, other);
        best_val = std::max(best_val, val);
        alpha = std::max(alpha, val);
        if(alpha >= beta) // cut-off
            return beta;
    }

    return best_val;
}
